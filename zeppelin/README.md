# Zeppelin

## Dockerfile

The `Dockerfile` is used to build a container that can run with Spark using Kubernetes as the scheduler.

## Standalone

Use the `docker-compose.yaml` to use the public Zeppelin image in a standalone mode.
