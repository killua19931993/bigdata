# Zookeeper (standalone)

Start a Zookeeper with docker-compose


```bash
make
```

Test your setup from your local computer with kafka installed locally.

```bash
docker run -ti --network=hadoop registry.gitlab.com/pod-o-mat/bigdata/zookeeper:3.6.0-001 /opt/zookeeper/bin/zkCli.sh -server zookeeper:2181
ls /
create /zk-test
ls /
create /zk-test/value 'i was here'
get /zk-test/value
delete /zk-test/value
delete /zk-test
ls /
```




Start a Zookeeper server on kubernetes in standalone mode.

## First step is to start Zookeeper

```bash
kubectl -n zookeeper apply -f k8s/zookeeper.yaml
```

## Now play with Zookeeper

To start an Zookeeper shell

```bash
kubectl run zookeeper-terminal -n zookeeper --rm --image=registry.gitlab.com/pod-o-mat/bigdata/zookeeper --restart=Never --tty -i --generator=run-pod/v1 -- /opt/zookeeper/bin/zkCli.sh -server zookeeper.zookeeper:2181
ls /
create /zk-test
ls /
create /zk-test/value 'i was here'
get /zk-test/value
delete /zk-test/value
delete /zk-test
ls /
```


## Cleanup

```bash
kubectl delete -f k8s/zookeeper.yaml
```

