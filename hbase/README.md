# HBase (standalone)

# Start an HBase with docker-compose

```bash
make
```

To test hbase

```bash
docker run -ti --network=hadoop registry.gitlab.com/pod-o-mat/bigdata/hbase:1.3.6 /bin/bash -c '\
	sed -e "s/ZOOKEEPER_PORT/2181/" -i.port.orig /opt/hbase/conf/hbase-site.xml && \
	sed -e "s/ZOOKEEPER_HOST/zookeeper/" -i.host.orig /opt/hbase/conf/hbase-site.xml && \
	/opt/hbase/bin/hbase shell'

status
create 'test', 'cf'
list 'test'
describe 'test'
put 'test', 'row1', 'cf:a', 'value1'
put 'test', 'row2', 'cf:a', 'value2'
put 'test', 'row3', 'cf:a', 'value3'
scan 'test'
get 'test', 'row1'
disable 'test'
enable 'test'
disable 'test'
drop 'test'
```


# Start an HBase server on kubernetes in standalone mode.

## First step is to start Zookeeper

```bash
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo update
helm install --name zookeeper --namespace zookeeper --version 5.1.0 bitnami/zookeeper
```

## Second step is to start HBase

```bash
kubectl apply -f k8s/hbase.yaml
```

## Now play with HBase

To start an HBase shell

```bash
kubectl run hbase-terminal --rm --image=registry.gitlab.com/pod-o-mat/bigdata/hbase --restart=Never --tty -i --generator=run-pod/v1 -- /bin/sh -c 'sed -e "s/ZOOKEEPER_PORT/2181/" -i.port.orig /opt/hbase/conf/hbase-site.xml && sed -e "s/ZOOKEEPER_HOST/zookeeper.zookeeper/" -i.host.orig /opt/hbase/conf/hbase-site.xml && /opt/hbase/bin/hbase shell'
```

To create a simple table named `test`

```hbase-cli
status
create 'test', 'cf'
list 'test'
describe 'test'
put 'test', 'row1', 'cf:a', 'value1'
put 'test', 'row2', 'cf:a', 'value2'
put 'test', 'row3', 'cf:a', 'value3'
scan 'test'
get 'test', 'row1'
disable 'test'
enable 'test'
disable 'test'
drop 'test'
```

A more complete example

```hbase-cli
status
create 'events', 'metadata', 'data'
list 'events'
describe 'events'
put 'events', '54321:id:com.ruggedcode.events.test:2019-10-31:0793B560-5943-4C57-9205-A71A0327532D', 'metadata:subject',         '12345'
put 'events', '54321:id:com.ruggedcode.events.test:2019-10-31:0793B560-5943-4C57-9205-A71A0327532D', 'metadata:subjecttype',     'id'
put 'events', '54321:id:com.ruggedcode.events.test:2019-10-31:0793B560-5943-4C57-9205-A71A0327532D', 'metadata:type',            'com.ruggedcode.events.test'
put 'events', '54321:id:com.ruggedcode.events.test:2019-10-31:0793B560-5943-4C57-9205-A71A0327532D', 'metadata:source',          'com.ruggedcode.hbase.shell'
put 'events', '54321:id:com.ruggedcode.events.test:2019-10-31:0793B560-5943-4C57-9205-A71A0327532D', 'metadata:date',            '2019-10-31 18:15:23 EST'
put 'events', '54321:id:com.ruggedcode.events.test:2019-10-31:0793B560-5943-4C57-9205-A71A0327532D', 'metadata:datacontenttype', 'text/plain'
put 'events', '54321:id:com.ruggedcode.events.test:2019-10-31:0793B560-5943-4C57-9205-A71A0327532D', 'data:message',             'Booohooooo!'

put 'events', '54321:id:com.ruggedcode.events.test:2019-11-01:E9FF90F7-8494-4EE6-8287-46E2B77884DF', 'metadata:subject',         '12345'
put 'events', '54321:id:com.ruggedcode.events.test:2019-11-01:E9FF90F7-8494-4EE6-8287-46E2B77884DF', 'metadata:subjecttype',     'id'
put 'events', '54321:id:com.ruggedcode.events.test:2019-11-01:E9FF90F7-8494-4EE6-8287-46E2B77884DF', 'metadata:type',            'com.ruggedcode.events.test'
put 'events', '54321:id:com.ruggedcode.events.test:2019-11-01:E9FF90F7-8494-4EE6-8287-46E2B77884DF', 'metadata:source',          'com.ruggedcode.hbase.shell'
put 'events', '54321:id:com.ruggedcode.events.test:2019-11-01:E9FF90F7-8494-4EE6-8287-46E2B77884DF', 'metadata:date',            '2019-11-01 08:18:00 EST'
put 'events', '54321:id:com.ruggedcode.events.test:2019-11-01:E9FF90F7-8494-4EE6-8287-46E2B77884DF', 'metadata:datacontenttype', 'text/plain'
put 'events', '54321:id:com.ruggedcode.events.test:2019-11-01:E9FF90F7-8494-4EE6-8287-46E2B77884DF', 'data:message',             'Hello world'

put 'events', '10000:id:com.ruggedcode.events.test:2019-11-02:6AA5322B-889E-44CA-8EBF-FA82A5127846', 'metadata:subject',         '00001'
put 'events', '10000:id:com.ruggedcode.events.test:2019-11-02:6AA5322B-889E-44CA-8EBF-FA82A5127846', 'metadata:subjecttype',     'id'
put 'events', '10000:id:com.ruggedcode.events.test:2019-11-02:6AA5322B-889E-44CA-8EBF-FA82A5127846', 'metadata:type',            'com.ruggedcode.events.test'
put 'events', '10000:id:com.ruggedcode.events.test:2019-11-02:6AA5322B-889E-44CA-8EBF-FA82A5127846', 'metadata:source',          'com.ruggedcode.hbase.shell'
put 'events', '10000:id:com.ruggedcode.events.test:2019-11-02:6AA5322B-889E-44CA-8EBF-FA82A5127846', 'metadata:date',            '2019-11-02 14:18:05 EST'
put 'events', '10000:id:com.ruggedcode.events.test:2019-11-02:6AA5322B-889E-44CA-8EBF-FA82A5127846', 'metadata:datacontenttype', 'text/plain'
put 'events', '10000:id:com.ruggedcode.events.test:2019-11-02:6AA5322B-889E-44CA-8EBF-FA82A5127846', 'data:message',             'Bob was here'

put 'events', '54321:id:com.ruggedcode.events.test:2019-11-02:D1BD05FC-5903-43CF-9F84-D4435D428913', 'metadata:subject',         '12345'
put 'events', '54321:id:com.ruggedcode.events.test:2019-11-02:D1BD05FC-5903-43CF-9F84-D4435D428913', 'metadata:subjecttype',     'id'
put 'events', '54321:id:com.ruggedcode.events.test:2019-11-02:D1BD05FC-5903-43CF-9F84-D4435D428913', 'metadata:type',            'com.ruggedcode.events.test'
put 'events', '54321:id:com.ruggedcode.events.test:2019-11-02:D1BD05FC-5903-43CF-9F84-D4435D428913', 'metadata:source',          'com.ruggedcode.hbase.shell'
put 'events', '54321:id:com.ruggedcode.events.test:2019-11-02:D1BD05FC-5903-43CF-9F84-D4435D428913', 'metadata:date',            '2019-11-02 14:18:10 EST'
put 'events', '54321:id:com.ruggedcode.events.test:2019-11-02:D1BD05FC-5903-43CF-9F84-D4435D428913', 'metadata:datacontenttype', 'text/plain'
put 'events', '54321:id:com.ruggedcode.events.test:2019-11-02:D1BD05FC-5903-43CF-9F84-D4435D428913', 'data:message',             'Hello boby'

scan 'events', {COLUMN=>'data:message'}
scan 'events', {REVERSED => true, COLUMN=>'data:message'}

scan 'events', {ROWPREFIXFILTER=>'54321:id:com.ruggedcode.events.test:2019'}
scan 'events', {ROWPREFIXFILTER=>'54321:id:com.ruggedcode.events.test:2019', COLUMN=>'data:message'}
scan 'events', {ROWPREFIXFILTER=>'54321:id:com.ruggedcode.events.test:2019', COLUMN=>'data:message', LIMIT => 2}
scan 'events', {ROWPREFIXFILTER=>'54321:id:com.ruggedcode.events.test:2019-11', COLUMN=>'data:message', LIMIT => 1}

get 'events', '54321:id:com.ruggedcode.events.test:2019-11-02:D1BD05FC-5903-43CF-9F84-D4435D428913'

disable 'events'
drop 'events'
```

## Zookeeper

If you want to see what is in the zookeeper distributed key value store for HBase

```bash
kubectl run zookeeper-terminal --rm --image=bitnami/zookeeper:3.5.6-debian-9-r0 --restart=Never --tty -i --generator=run-pod/v1 -- /opt/bitnami/zookeeper/bin/zkCli.sh -server zookeeper.zookeeper:2181
get /hbase/master
```


## Cleanup

```bash
kubectl delete -f k8s/hbase.yaml
helm delete zookeeper --purge
```

