#!/bin/sh -e

# This can run before sshd is running
if [ ! -r /tmp/hadoop-hadoop/hdfs-formated ] ; then 
	echo "root about to initialize hdfs"
	sudo -H -i -u hadoop /opt/hadoop/initialize-hdfs.sh
fi

# Wait for SSHD to start HDFS and YARN
(
	printf "Waiting for sshd ."
	until ssh -Ao 'StrictHostKeyChecking no' localhost -o 'BatchMode=yes' -o 'ConnectionAttempts=1' true ; do
		printf "."
		sleep 1
	done
	printf " [OK]\n"
	(sudo -H -i -u hadoop /opt/hadoop/sbin/start-dfs.sh) &
	(sudo -H -i -u hadoop /opt/hadoop/sbin/start-yarn.sh) &
) &

/bin/watchgod -- /usr/sbin/sshd -D