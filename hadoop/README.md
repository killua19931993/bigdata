# Hadoop

## Test HDFS


Login as `hadoop` and create a directory called 'test'
```bash
docker exec -ti hadoop sudo -H -i -u hadoop
/opt/hadoop/bin/hdfs dfs -mkdir hdfs://localhost:9000/test
/opt/hadoop/bin/hdfs dfs -ls hdfs://localhost:9000/
```

To create a directory and change the owner to `root` and then create a subdirectory `new-dir` as the user `root`

```bash 
sudo -H -i -u hadoop /opt/hadoop/bin/hdfs dfs -mkdir hdfs://localhost:9000/root
root@hadoop:/opt/hadoop# sudo -H -i -u hadoop /opt/hadoop/bin/hdfs dfs -chown root hdfs://localhost:9000/root

/opt/hadoop/bin/hdfs dfs -mkdir hdfs://localhost:9000/root/new-dir
/opt/hadoop/bin/hdfs dfs -ls hdfs://localhost:9000/root/
```

Or less secure, make the root filesystem writeable to everyone, admin Party!

```bash

sudo -H -i -u hadoop /opt/hadoop/bin/hdfs dfs -chmod 777 hdfs://localhost:9000/
/opt/hadoop/bin/hdfs dfs -mkdir hdfs://localhost:9000/anyone-can-write-here
/opt/hadoop/bin/hdfs dfs -ls hdfs://localhost:9000/
```

To open the [HDFS web ui](http://localhost:9870) and the [yarn manager](http://localhost:8088)

