# Kafka (standalone)

## Docker Compose SETUP


### To use kafka from you computer.

```bash
make KAFKA=localhost
```

Test your setup from your local computer with kafka installed locally.

```bash
$ bin/kafka-topics.sh --create --bootstrap-server 127.0.0.1:9092 --replication-factor 1 --partitions 1 --topic test-kafka
$ bin/kafka-topics.sh --list --bootstrap-server 127.0.0.1:9092
$ bin/kafka-console-producer.sh --broker-list 127.0.0.1:9092 --topic test
	First message
	second message
	hello world!
	^D
$ bin/kafka-console-consumer.sh --bootstrap-server 127.0.0.1:9092 --topic test --from-beginning
	First message
	second message
	hello world!
	^C
```

### To use kafka from you computer and from other docker containers.

To use kafka from other containers. If you also want to use
kafka from your local computer you will have to add
`127.0.0.1 kafka` to your `/etc/hosts` file.


```bash
make KAFKA=kafka
```

Test your setup from your local computer with kafka installed locally.

```bash
$ bin/kafka-topics.sh --create --bootstrap-server kafka:9092 --replication-factor 1 --partitions 1 --topic test-kafka
$ bin/kafka-topics.sh --list --bootstrap-server kafka:9092
$ bin/kafka-console-producer.sh --broker-list kafka:9092 --topic test
	First message
	second message
	hello world!
	^D
$ bin/kafka-console-consumer.sh --bootstrap-server kafka:9092 --topic test --from-beginning
	First message
	second message
	hello world!
	^C
```


## Kubernetes  SETUP

Start a Kafka broker on kubernetes in standalone mode.

## First step is to start Zookeeper

```bash
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo update
helm install --name zookeeper --namespace zookeeper --version 5.1.0 bitnami/zookeeper
```

## Second step is to start Kafka

```bash
kubectl apply -f k8s/kafka.yaml
```

## Now play with Kafka

To start an Kafka shell

```bash

kubectl run kafka-terminal -n default --rm --image=registry.gitlab.com/pod-o-mat/bigdata/kafka --restart=Never --tty -i --generator=run-pod/v1 -- /opt/kafka/bin/kafka-topics.sh --create --bootstrap-server kafka-0.bootstrap-server.kafka:9092 --replication-factor 1 --partitions 1 --topic test

kubectl run kafka-terminal -n default --rm --image=registry.gitlab.com/pod-o-mat/bigdata/kafka --restart=Never --tty -i --generator=run-pod/v1 -- /opt/kafka/bin/kafka-topics.sh --list --bootstrap-server kafka-0.bootstrap-server.kafka:9092


kubectl run kafka-terminal -n default --rm --image=registry.gitlab.com/pod-o-mat/bigdata/kafka --restart=Never --tty -i --generator=run-pod/v1 -- /opt/kafka/bin/kafka-console-producer.sh --broker-list kafka-0.bootstrap-server.kafka:9092 --topic test
This is a message
This is another message
^D

kubectl run kafka-terminal -n default --rm --image=registry.gitlab.com/pod-o-mat/bigdata/kafka --restart=Never --tty -i --generator=run-pod/v1 -- /opt/kafka/bin/kafka-console-consumer.sh --bootstrap-server kafka-0.bootstrap-server.kafka:9092 --topic test --from-beginning
This is a message
This is another message
^C
```
## Zookeeper

If you want to see what is in the zookeeper distributed key value store for Kafka

```bash
kubectl run zookeeper-terminal --rm --image=registry.gitlab.com/pod-o-mat/bigdata/zookeeper --restart=Never --tty -i --generator=run-pod/v1 -- /opt/zookeeper/bin/zkCli.sh -server zookeeper.zookeeper:2181
ls /brokers/topics
get /brokers/topics/test
```


## Cleanup

```bash
kubectl delete -f k8s/kafka.yaml
helm delete zookeeper --purge
```

